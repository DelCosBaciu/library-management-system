package com.deloittedigital.library.repository;

import com.deloittedigital.library.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    //TODO: find the user with a specific email
}
